/**
 * Magento
 *
 * Mini Cart Header
 * Author: Hire Magento
 * Website: www.hiremagento.com 
 * Suport Email: hiremagento@gmail.com
 *
**/
jQuery(window).load(function () {
	if(jQuery("#minicart")){
		jQuery("#minicart").mouseover(function() {
	  		jQuery('minicart-panel').show();
		});
		jQuery("#minicart").mouseout(function() {
	  		jQuery('minicart-panel').hide();
		});
	}
});