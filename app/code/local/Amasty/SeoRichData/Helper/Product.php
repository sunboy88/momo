<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) Amasty (http://www.amasty.com)
 */

class Amasty_SeoRichData_Helper_Product extends Mage_Core_Helper_Abstract
{
    const ITEM_TYPE_OFFER_URL        = 'http://data-vocabulary.org/Offer';

    protected $_priceApplied = false;

    public function applyPrice($html)
    {
        if ($this->_priceApplied)
            return $html;

        $priceSelector = Mage::getStoreConfig('amseorichdata/product/container_price_selector');
        $specialPriceSelector = Mage::getStoreConfig('amseorichdata/product/price_special_selector');
        $regularSelector = Mage::getStoreConfig('amseorichdata/product/price_regular_selector');

        $dom = new Amasty_SeoRichData_Model_Dom($html);

        $isSpecial = false;
        $priceNode = null;

        if ($priceNode = $dom->query($specialPriceSelector))
            $isSpecial = true;

        if (!$isSpecial)
            $priceNode = $dom->query($regularSelector);

        if ($priceNode)
        {
            if ($priceBox = $dom->query($priceSelector))
            {
                $priceBox->setAttribute('itemscope', '');
                $priceBox->setAttribute('itemtype', self::ITEM_TYPE_OFFER_URL);
                $priceBox->setAttribute('itemprop', 'offerDetails');
                $priceBox->setAttribute('itemref', 'availability');
            }

            $html = $dom->save();
            $html = html_entity_decode($html, ENT_QUOTES, "UTF-8");

            $oldValue = html_entity_decode($priceNode->nodeValue, ENT_QUOTES, "UTF-8");

            if ($newPriceHtml = $this->_wrapPrice($priceNode->nodeValue, $isSpecial))
            {
                $html = str_replace($oldValue, $newPriceHtml, $html);
            }

            $this->_priceApplied = true;
        }

        return $html;
    }

    protected function _wrapPrice($text, $isSpecial)
    {
        $tag = Mage::helper('amseorichdata/htmlManager')->getCustomTag();
        $priceHtml    = '';
        $currencyCode = null;
        $price        = trim($text);
        $priceInfo    = preg_split('/(^\D+)/', $price, - 1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        if (count($priceInfo) != 2) {
            $priceInfo = preg_split('/(\D+)$/', $price, - 1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
            if (count($priceInfo) == 2) {
                if ($priceInfo[0]) {
                    $priceHtml .= "<{$tag} itemprop='price'>{$priceInfo[0]}</{$tag}>";
                }

                if ($priceInfo[1]) {
                    $code = Mage::app()->getStore()->getCurrentCurrencyCode();
                    $priceHtml .= "<{$tag} itemprop='currency' content='$code'>{$priceInfo[1]}</{$tag}>";
                }
            }
        } else {
            if ($priceInfo[0]) {
                $code = Mage::app()->getStore()->getCurrentCurrencyCode();
                $priceHtml .= "<{$tag} itemprop='currency' content='$code'>{$priceInfo[0]}</{$tag}>";
            }

            if ($priceInfo[1]) {
                $priceHtml .= "<{$tag} itemprop='price'>{$priceInfo[1]}</{$tag}>";
            }
        }

        if ($isSpecial) {
            $_product = null;
            if (! empty($priceHtml) &&
                (($_product = Mage::registry('current_product')) || ($_product = Mage::registry('product')))
            ) {
                if ($_product->getSpecialToDate()) {
                    $priceHtml .= '<time itemprop="priceValidUntil" datetime="' .
                        date('Y-m-d', strtotime($_product->getSpecialToDate())) . '" />';
                }
            }

            $this->_priceApplied = ! empty($priceHtml);
        }

        return $priceHtml;
    }

    public function applyImage($html)
    {
        $pattern = '|(\<img)([^>]+src=[^>]+/image/[^>]+\>)|i';
        $replace = '${1} itemprop="image" ${2}';

        return preg_replace($pattern, $replace, $html, 1);
    }
}
