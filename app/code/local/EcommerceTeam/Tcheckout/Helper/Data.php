<?php

class EcommerceTeam_Tcheckout_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * @param $xmlnode
     * @return mixed
     */
    public function getConfigData($xmlnode){
    	
    	if(!isset($this->_config_cache[$xmlnode])){
    		$this->_config_cache[$xmlnode] = Mage::getStoreConfig('checkout/'.$xmlnode);
    	}
    	return $this->_config_cache[$xmlnode];
	}

    /**
     * @param $xmlnode
     * @return mixed
     */
    public function getConfigFlag($xmlnode){
    	
    	if(!isset($this->_config_cache[$xmlnode])){
    		$this->_config_cache[$xmlnode] = Mage::getStoreConfigFlag('checkout/'.$xmlnode);
    	}
    	return $this->_config_cache[$xmlnode];
	}

    /**
     * @return bool
     */
    public function showSubscribe()
    {
        $result = $this->getConfigFlag('options/subscribe_enabled');
        /** @var $session Mage_Customer_Model_Session */
        $session = Mage::getSingleton('customer/session');
        if ($result && $session->isLoggedIn()) {
            /** @var $subscribeModel Mage_Newsletter_Model_Subscriber */
            $subscribeModel = Mage::getModel('newsletter/subscriber');
            $subscribeModel->loadByCustomer($session->getCustomer());
            return !$subscribeModel->getStatus();
        }
        return $result;
    }
}
