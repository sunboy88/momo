<?php
class NextBits_Wholesale_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/wholesale?id=15 
    	 *  or
    	 * http://site.com/wholesale/id/15 	
    	 */
    	/* 
		$wholesale_id = $this->getRequest()->getParam('id');

  		if($wholesale_id != null && $wholesale_id != '')	{
			$wholesale = Mage::getModel('wholesale/wholesale')->load($wholesale_id)->getData();
		} else {
			$wholesale = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($wholesale == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$wholesaleTable = $resource->getTableName('wholesale');
			
			$select = $read->select()
			   ->from($wholesaleTable,array('wholesale_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$wholesale = $read->fetchRow($select);
		}
		Mage::register('wholesale', $wholesale);
		*/

			
		$this->loadLayout();     
		$this->renderLayout();
    }
	public function testAction()
	{		
		$websiteData = Array(
			'name' => 'Wholesale Website' ,
			'code' => 'wholesale_website' ,   
		);

		$websiteModel = Mage::getModel('core/website');
		$websiteModel->setData($websiteData);
		$websiteModel->save();

		$websiteModel1 = Mage::getModel('core/website')->getCollection()->getData();
		foreach($websiteModel1 as $webModel) {
			if($webModel['code'] == 'wholesale_website') {
				$websiteId = $webModel['website_id'];
			}
		}

		$rootCategoryId = Mage::app()->getStore()->getRootCategoryId();
		$groupData = Array(
			'website_id' => $websiteId,
			'name' => 'Wholesale Store',
			'root_category_id' => $rootCategoryId,    
		);

		$groupModel = Mage::getModel('core/store_group');
		$groupModel->setData($groupData);
		$groupModel->save();
		
		$groupModel1 = Mage::getModel('core/store_group')->getCollection()->getData();
		foreach($groupModel1 as $grpModel) {
			if($grpModel['website_id'] == $websiteId) {
				$group_id = $grpModel['group_id'];
			}
		}

		$storeData = Array
		(
			'group_id' => $group_id,
			'website_id' => $websiteId,
			'name' => 'Wholesale Store View',
			'code' => 'wholesale_store_view',
			'is_active' => 1,	
		);

		$storeModel = Mage::getModel('core/store');
		$storeModel->setData($storeData);
		$storeModel->save();
		
		$config = new Mage_Core_Model_Config();
		$config->saveConfig('customer/customeractivation/disable_ext', 1, 'default', 0);
		$config->saveConfig('customer/customeractivation/always_active_in_admin', 1, 'default', 0);
		$config->saveConfig('customer/customeractivation/disable_ext', 0, 'websites', $websiteId);

		$config->saveConfig('catalog/logincatalog/disable_ext', 1, 'default', 0);
		$config->saveConfig('catalog/logincatalog/hide_categories', 1, 'default', 0);
		$config->saveConfig('catalog/logincatalog/disable_ext', 0, 'websites', $websiteId);
		
		$baseURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
		
		$config->saveConfig('web/unsecure/base_url', $baseURL, 'default', 0);
		$config->saveConfig('web/unsecure/base_url', $baseURL.'wholesale/', 'websites', $websiteId);
		$config->saveConfig('web/unsecure/base_link_url', '{{unsecure_base_url}}', 'default', 0);
		$config->saveConfig('web/unsecure/base_link_url', '{{unsecure_base_url}}', 'websites', $websiteId);
		$config->saveConfig('web/unsecure/base_skin_url', '{{unsecure_base_url}}skin/', 'default', 0);
		$config->saveConfig('web/unsecure/base_skin_url', '{{unsecure_base_url}}../skin/', 'websites', $websiteId);
		$config->saveConfig('web/unsecure/base_media_url', '{{unsecure_base_url}}media/', 'default', 0);
		$config->saveConfig('web/unsecure/base_media_url', '{{unsecure_base_url}}../media/', 'websites', $websiteId);
		$config->saveConfig('web/unsecure/base_js_url', '{{unsecure_base_url}}js/', 'default', 0);
		$config->saveConfig('web/unsecure/base_js_url', '{{unsecure_base_url}}../js/', 'websites', $websiteId);

		$config->saveConfig('web/secure/base_url', $baseURL, 'default', 0);
		$config->saveConfig('web/secure/base_url', $baseURL.'wholesale/', 'websites', $websiteId);
		$config->saveConfig('web/secure/base_link_url', '{{secure_base_url}}', 'default', 0);
		$config->saveConfig('web/secure/base_link_url', '{{secure_base_url}}', 'websites', $websiteId);
		$config->saveConfig('web/secure/base_skin_url', '{{secure_base_url}}skin/', 'default', 0);
		$config->saveConfig('web/secure/base_skin_url', '{{secure_base_url}}../skin/', 'websites', $websiteId);
		$config->saveConfig('web/secure/base_media_url', '{{secure_base_url}}media/', 'default', 0);
		$config->saveConfig('web/secure/base_media_url', '{{secure_base_url}}../media/', 'websites', $websiteId);
		$config->saveConfig('web/secure/base_js_url', '{{secure_base_url}}js/', 'default', 0);
		$config->saveConfig('web/secure/base_js_url', '{{secure_base_url}}../js/', 'websites', $websiteId);

		$config->saveConfig('design/theme/layout', Mage::getStoreConfig('design/theme/layout'), 'default', 0);
		$config->saveConfig('design/theme/layout', 'wholesale', 'websites', $websiteId);
		$config->saveConfig('design/theme/template', Mage::getStoreConfig('design/theme/template'), 'default', 0);
		$config->saveConfig('design/theme/template', 'wholesale', 'websites', $websiteId);

		$config->saveConfig('customer/address/taxvat_show', Mage::getStoreConfig('customer/address/taxvat_show'), 'default', 0);
		$config->saveConfig('customer/address/taxvat_show', 'req', 'websites', $websiteId);

		$config->saveConfig('catalog/hideprices/hide_prices_for_guests', 0, 'default', 0);
		$config->saveConfig('catalog/hideprices/hide_prices_for_guests', 1, 'websites', $websiteId);
		$config->saveConfig('catalog/hideprices/hide_addtocart_for_guests', 0, 'default', 0);
		$config->saveConfig('catalog/hideprices/hide_addtocart_for_guests', 1, 'websites', $websiteId);

		/* $config->saveConfig('tax/calculation/algorithm', 'TOTAL_BASE_CALCULATION', 'websites', $websiteId);
		$config->saveConfig('tax/calculation/based_on', 'origin', 'websites', $websiteId);
		$config->saveConfig('tax/calculation/price_includes_tax', '0', 'websites', $websiteId);
		$config->saveConfig('tax/calculation/shipping_includes_tax', '0', 'websites', $websiteId);
		$config->saveConfig('tax/calculation/apply_after_discount', '1', 'websites', $websiteId);
		$config->saveConfig('tax/calculation/discount_tax', '0', 'websites', $websiteId);
		$config->saveConfig('tax/calculation/apply_tax_on', '0', 'websites', $websiteId);
		$config->saveConfig('tax/display/type', '1', 'websites', $websiteId);
		$config->saveConfig('tax/display/shipping', '3', 'websites', $websiteId);
		$config->saveConfig('tax/cart_display/price', '1', 'websites', $websiteId);
		$config->saveConfig('tax/cart_display/subtotal', '1', 'websites', $websiteId);
		$config->saveConfig('tax/cart_display/shipping', '1', 'websites', $websiteId);
		$config->saveConfig('tax/cart_display/grandtotal', '1', 'websites', $websiteId);
		$config->saveConfig('tax/cart_display/full_summary', '1', 'websites', $websiteId);
		$config->saveConfig('tax/cart_display/zero_tax', '1', 'websites', $websiteId);
		$config->saveConfig('tax/sales_display/price', '1', 'websites', $websiteId);
		$config->saveConfig('tax/sales_display/subtotal', '1', 'websites', $websiteId);
		$config->saveConfig('tax/sales_display/shipping', '1', 'websites', $websiteId);
		$config->saveConfig('tax/sales_display/grandtotal', '1', 'websites', $websiteId);
		$config->saveConfig('tax/sales_display/full_summary', '1', 'websites', $websiteId);
		$config->saveConfig('tax/sales_display/zero_tax', '1', 'websites', $websiteId);
 */
		$config->saveConfig('catalog/price/scope', '1', 'default', 0);
		
		Mage::getConfig()->reinit();
		Mage::app()->reinitStores();
		Mage::getConfig()->cleanCache();
		
		// Useful for automatic re-indexing all the indexes...
		/* $indexingProcesses = Mage::getSingleton('index/indexer')->getProcessesCollection(); 
		foreach ($indexingProcesses as $process) {
			  $process->reindexEverything();
		} */
		//////////////////////////////////////////////

		/* $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
		$setup->startSetup();

		$entityTypeId     = $setup->getEntityTypeId('customer');
		$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
		$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

		$setup->addAttribute('customer', 'kvk_nummer', array(
			'input'         => 'text',
			'type'          => 'varchar',
			'label'         => 'KvK nummer',
			'visible'       => 1,
			'required'      => 0,
			'user_defined' => 1,
		));

		$setup->addAttributeToGroup(
		 $entityTypeId,
		 $attributeSetId,
		 $attributeGroupId,
		 'kvk_nummer',
		 '999'  //sort_order
		);

		$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'kvk_nummer');
		//$oAttribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit','checkout_register'));
		$oAttribute->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit'));
		$oAttribute->save();

		$setup->endSetup(); */
		
		$this->_redirectReferer();
	}
}