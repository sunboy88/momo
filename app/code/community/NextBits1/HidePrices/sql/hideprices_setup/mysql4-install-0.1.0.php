<?php
/**
 * Red2Black HidePrices
 *
 * Magento extension
 *
 * NOTICE OF LICENSE
 * Magento is subject to the Open Software License (OSL 3.0)
 *
 * @author      Andre Nickatina<andre@r2bconcepts.com>
 * @category    Red2Black
 * @package     Red2Black_HidePrices
 * @copyright   R2Bconcepts.com 2011
 * @link        http://www.r2bconcepts.com Red2Black Concepts
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version     0.1.0
*/

$installer = $this;

$installer->startSetup();

$productTypes = 'simple,configurable,virtual,bundle,downloadable';

$installer->createAttribute('r2b_hide_price_from_guests', 'Hide price from guests?', 'boolean', $productTypes);
$installer->createAttribute('r2b_call_for_price', 'Call for price enabled?', 'boolean', $productTypes);
$installer->createAttribute('r2b_add_to_see_price', 'Add to cart to see price?', 'boolean', $productTypes);


$installer->endSetup();
