<?php

/**
 * Adminhtml sales order export to pdf
 *
 * @category   Webinse
 * @package    Webinse_OrderPdf
 * @author     Webinse Team <alena.tsareva@webinse.com>
 */
class Webinse_OrderPdf_Block_Adminhtml_Sales_Order_Export_Pdf extends Mage_Adminhtml_Block_Sales_Order_View_Items_Renderer_Default{

    protected $_visibleItems = array();

    protected function _getVisibleItems(){
        if(!$this->_visibleItems){
            $this->_visibleItems = Mage::helper('wb_orderpdf')->getOrderVisibleBlocks();
        }

        return $this->_visibleItems;
    }

    protected function _getOrderStoreName(Mage_Sales_Model_Order $order){
        $storeId = $order->getStoreId();
        if(is_null($storeId)){
            $deleted = Mage::helper('adminhtml')->__(' [deleted]');
            return nl2br($order->getStoreName()) . $deleted;
        }
        $store = Mage::app()->getStore($storeId);
        $name = array(
            $store->getWebsite()->getName(),
            $store->getGroup()->getName(),
            $store->getName()
        );

        return implode('<br/>', $name);
    }

    protected function _getCustomerGroupName(Mage_Sales_Model_Order $order){
        return Mage::getModel('customer/group')->load((int)$order->getCustomerGroupId())->getCode();
    }

    /**
     * Check if visible item (order information block, set in the system configuration)
     *
     * @param string $itemKey
     * @return boolean
     */
    public function isVisibleItem($itemKey = false){
        $visibleItems = $this->_getVisibleItems();
        if($itemKey){
            if(key_exists($itemKey, $visibleItems)){
                return true;
            }
        } else{
            if(!is_bool($visibleItems) && count($visibleItems) > 0){
                return true;
            }
        }

        return false;
    }

    /**
     * Rerieve item name by key
     *
     * @param string $itemKey
     * @return string
     */
    public function getItemName($itemKey){
        $visibleItems = $this->_getVisibleItems();
        if(isset($visibleItems[$itemKey])){
            return $visibleItems[$itemKey]['item_name'];
        }

        return '';
    }

    public function getBaseInfo(Mage_Sales_Model_Order $order){
        $info = array(
            'Order ID' => $order->getRealOrderId(),
            'Order Date' => Mage::helper('core')->formatDate($order->getCreatedAt(), 'medium', true),
            'Order Status' => $order->getStatusLabel(),
            'Purchased From' => $this->_getOrderStoreName($order),
            'Placed from IP' => $order->getRemoteIp() . (($order->getXForwardedFor()) ? ' (' . $this->escapeHtml($order->getXForwardedFor()) . ')' : '')
        );

        return $info;
    }

    public function getAccountInfo(Mage_Sales_Model_Order $order){
        $info = array(
            'Customer Name' => $order->getCustomerName(),
            'Email' => $order->getCustomerEmail(),
        );

        if($customerGroup = $this->_getCustomerGroupName($order)){
            $info['Customer Group'] = $customerGroup;
        }

        return $info;
    }

    public function getPaymentInfo(Mage_Sales_Model_Order $order){
        return Mage::helper('payment')->getInfoBlock($order->getPayment())->_toHtml();
    }

    /**
     * Retrieve order items collection
     *
     * @return unknown
     */
    public function getItemsCollection(){
        return $this->getOrder()->getItemsCollection();
    }

    /**
     * Retrieve Gift Message by id
     *
     * @param Mage_Sales_Model_Order $order
     * @return string
     */
    public function getGiftMessage(Mage_Sales_Model_Order $order){
        return Mage::helper('giftmessage/message')->getGiftMessage($order->getGiftMessageId())->getMessage();
    }

    /**
     * Retrieve subtotal price include tax html formated content
     *
     * @param Mage_Sales_Model_Order $item
     * @return string
     */
    public function displayShippingPriceInclTax(Mage_Sales_Model_Order $order){
        $shipping = $order->getShippingInclTax();
        if($shipping){
            $baseShipping = $order->getBaseShippingInclTax();
        } else{
            $shipping = $order->getShippingAmount() + $order->getShippingTaxAmount();
            $baseShipping = $order->getBaseShippingAmount() + $order->getBaseShippingTaxAmount();
        }
        return $this->displayPrices($baseShipping, $shipping, false, ' ');
    }

    /**
     * Retrierve company information
     *
     * @return string
     */
    public function getCompanyInfo(){
        $html = '';
        $helper = Mage::helper('wb_orderpdf');

        if($value = $helper->getOrderPdfCompanyInfo('address')){
            $html .= '<p>' . $value . '</p>';
        }

        if($value = $helper->getOrderPdfCompanyInfo('email')){
            $html .= '<p><label>' . $helper->__('Email') . ':</label> ' . $value . '</p>';
        }

        if($value = $helper->getOrderPdfCompanyInfo('phone')){
            $html .= '<p><label>' . $helper->__('Phone') . ':</label> ' . $value . '</p>';
        }

        if($value = $helper->getOrderPdfCompanyInfo('fax')){
            $html .= '<p><label>' . $helper->__('Fax') . ':</label> ' . $value . '</p>';
        }

        if($value = $helper->getOrderPdfCompanyInfo('vat_number')){
            $html .= '<p><label>' . $helper->__('VAT Numbe') . ':</label> ' . $value . '</p>';
        }

        if($value = $helper->getOrderPdfCompanyInfo('vat_office')){
            $html .= '<p><label>' . $helper->__('VAT Office') . ':</label> ' . $value . '</p>';
        }

        return $html;
    }

    /**
     * Retrieve ordered items
     *
     * @return mixed
     */
    public function getOrderedItems(){
       // echo 'getOrderedItems';
        return $this->getLayout()->createBlock('adminhtml/sales_order_view_items', 'orderedItems')
            ->setTemplate('sales/order/view/allitems.phtml')
           // ->assign('order', Mage::registry('order'))
           // ->assign('orders', Mage::getSingleton('core/session')->getOrders())
            ->setParentBlock($this->getLayout()->getBlock('orderPdf'))
            //->addItemRender('default', 'adminhtml/sales_order_view_items_renderer_default', 'sales/order/view/items/renderer/default.phtml')
            ->toHtml();
    }
}
