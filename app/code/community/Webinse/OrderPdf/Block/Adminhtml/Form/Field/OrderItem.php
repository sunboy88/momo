<?php

/**
 * Adminhtml grid field example
 *
 * @category   Webinse
 * @package    Webinse_OrderPdf
 * @author     Webinse Team <alena.tsareva@webinse.com>
 */
class Webinse_OrderPdf_Block_Adminhtml_Form_Field_OrderItem extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    /**
     * @var Mage_CatalogInventory_Block_Adminhtml_Form_Field_Customergroup
     */
    protected $_blockRenderer;

    /**
     * Retrieve currency column renderer
     *
     * @return Webinse_ConfigGrid_Block_Adminhtml_Form_Field_Currency
     */
    protected function _getBlockRenderer()
    {
        if (!$this->_blockRenderer) {
            $this->_blockRenderer = $this->getLayout()->createBlock(
                    'wb_orderpdf/adminhtml_form_field_items', '', array('is_render_to_js_template' => true)
            );
            $this->_blockRenderer->setClass('select_example');
            $this->_blockRenderer->setExtraParams('style="width:100px"');
        }
        return $this->_blockRenderer;
    }

    /**
     * Prepare to render
     */
    protected function _prepareToRender()
    {
        $this->addColumn('order_item', array(
            'label'    => $this->__('Block'),
            'renderer' => $this->_getBlockRenderer(),
        ));
        $this->addColumn('item_name', array(
            'label' => Mage::helper('wb_orderpdf')->__('Custom Block Name'),
            'style' => 'width:100px',
        ));
        $this->_addAfter       = false;
        $this->_addButtonLabel = Mage::helper('wb_orderpdf')->__('Add');
    }

    /**
     * Prepare existing row data object
     *
     * @param Varien_Object
     */
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
                'option_extra_attr_' . $this->_getBlockRenderer()->calcOptionHash($row->getData('order_item')), 'selected="selected"'
        );
    }

}
