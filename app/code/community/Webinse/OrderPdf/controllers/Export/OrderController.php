<?php

/**
 * Controller handling order export requests.
 *
 * @category   Webinse
 * @package    Webinse_OrderPdf
 * @author     Webinse Team <alena.tsareva@webinse.com>
 */
class Webinse_OrderPdf_Export_OrderController extends Mage_Adminhtml_Controller_Action
{

    protected function _checkVisibleBlocks()
    {
        if (count(Mage::helper('wb_orderpdf')->getOrderVisibleBlocks()) == 0) {
            $this->_getSession()->addError($this->__('Please, specify Order Visible Blocks in the System->Configuration->Webinse->Order to PDF.'));
            $this->_redirect('*/*/');
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return false;
        }

        return true;
    }

    /**
     * Init selected orders
     *
     * @param array $orderIds
     * @return array
     */
    protected function _initOrders($orderIds)
    {
        $orders = array();

        if (is_array($orderIds)) {
            foreach ($orderIds as $orderId) {
                $order = Mage::getModel('sales/order')->load((int) $orderId);
                if ($order->getId()) {
                    $orders[$order->getId()] = $order;
                }
            }
        }
        else {
            $order = Mage::getModel('sales/order')->load((int) $orderIds);
            if ($order->getId()) {
                $orders[$order->getId()] = $order;
            }
        }

        return $orders;
    }

    /**
     * Initialize order model instance
     *
     * @return Mage_Sales_Model_Order || false
     */
    protected function _initOrder()
    {
        $id    = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($id);

        if (!$order->getId()) {
            $this->_getSession()->addError($this->__('This order no longer exists.'));
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return false;
        }

        Mage::register('sales_order', $order);
        Mage::register('current_order', $order);
        return $order;
    }

    protected function _getOrdersHtml($orders)
    {
        //echo 'test 122222';
        $childOrderTax = Mage::getModel('core/layout')
                ->createBlock('adminhtml/sales_order_totals_tax', 'tax', array('template' => 'sales/order/totals/tax.phtml'));

        $childOrderTotals = Mage::getModel('core/layout')
                ->createBlock('adminhtml/sales_order_totals', 'orderTotals', array('template' => 'orderpdf/sales/order/totals.phtml'))
                ->setChild('tax', $childOrderTax);

        $childOrderFooter = Mage::getModel('core/layout')
                ->createBlock('wb_orderpdf/adminhtml_sales_order_export_pdf', 'orderFooter', array('template' => 'orderpdf/sales/order/footer.phtml'))
                ->setChild('orderTotals', $childOrderTotals);

        $block = Mage::getModel('core/layout')
                ->createBlock('wb_orderpdf/adminhtml_sales_order_export_pdf', 'orderPdf', array('template' => 'orderpdf/sales/order/export/pdf.phtml'))
                ->assign('orders', $orders)
                ->setChild('orderTotals', $childOrderTotals)
                ->setChild('orderFooter', $childOrderFooter);
       // var_dump($block->toHtml());die('zzz');
        return $block->toHtml();
    }

    /**
     * Exports orders defined by id in post param "order_ids" to pdf and offers file directly for download
     * when finished.
     */
    public function exportOrdersAction()
    {
        if ($this->_checkVisibleBlocks()) {
            $orderIds = $this->getRequest()->getPost('order_ids', array());

            //init orders
            $orders = $this->_initOrders($orderIds);
          //  var_dump($orderIds);die('ssss');
            //retrieve orders html
            $html = $this->_getOrdersHtml($orders);

            //export to pdf
            Mage::getModel('wb_orderpdf/pdf')->export($html, 'Orders_' . date("Ymd_His"));
        }
        else {
            $this->_redirect('adminhtml/sales_order');
        }
    }

    /**
     * Export order to pdf and offers file directly for download when finished.
     */
    public function exportAction()
    {
        if ($this->_checkVisibleBlocks()) {
            if ($order = $this->_initOrder()) {
                $orders = array($order->getId() => $order);

                //retrieve orders html
                $html = $this->_getOrdersHtml($orders);

                //export to pdf
                Mage::getModel('wb_orderpdf/pdf')->export($html, 'Order_' . date("Ymd_His"));
            }
        }
        else {
            $this->_redirect('adminhtml/sales_order');
        }
    }

}

?>