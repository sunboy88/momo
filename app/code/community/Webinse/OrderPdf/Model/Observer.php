<?php

/**
 * Observer to append option to export to csv to mass action select box in the orders grid.
 *
 * @category   Webinse
 * @package    Webinse_OrderPdf
 * @author     Webinse Team <alena.tsareva@webinse.com>
 */
class Webinse_OrderPdf_Model_Observer
{

    /**
     * Extends the mass action select box to append the option to export to csv.
     * Event: core_block_abstract_prepare_layout_before
     */
    public function addMassaction($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if (get_class($block) == 'Mage_Adminhtml_Block_Widget_Grid_Massaction' && strpos($block->getRequest()->getControllerName(), 'sales_order') >= 0){
            $block->addItem('orderpdf', array(
                'label' => Mage::helper('core')->__('Export to .pdf file'),
                'url'   => Mage::app()->getStore()->getUrl('orderpdf/export_order/exportOrders'))
            );
        }
    }

    public function addExportPdfButton($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if (get_class($block) == 'Mage_Adminhtml_Block_Sales_Order_View' && strpos($block->getRequest()->getControllerName(), 'sales_order') >= 0) {
            $block->addButton('orderpdf', array(
                'label'   => Mage::helper('adminhtml')->__('Export to .pdf'),
                'class'   => 'orderpdf',
                'onclick'   => 'setLocation(\'' . $block->getUrl('orderpdf/export_order/export') . '\')',
            ));
        }
    }

}