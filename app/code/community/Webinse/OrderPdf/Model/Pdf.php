<?php

define('P_EXT', '.pdf');

/**
 * Pdf Model
 *
 * @category   Webinse
 * @package    Webinse_OrderPdf
 * @author     Webinse Team <alena.tsareva@webinse.com>
 */
class Webinse_OrderPdf_Model_Pdf extends Mage_Core_Model_Abstract
{

    /**
     * Export html to pdf
     *
     * @param html $html
     * @param string $documentName
     */
    public function export($html, $documentName)
    {
        //include pdf library
        include ('Webinse/OrderPdf/lib/mpdf/mpdf.php');

        //get page orientation
        $orientation = Mage::getStoreConfig('orderpdf/theme/page_orientation');

        //set format, indents, etc
        switch($orientation)
        {
            case 'L':
                $mpdf = new mPDF('utf-8', 'A4-L', '8', '', 10, 10, 7, 7, 10, 10);
                break;
            case 'P':
                $mpdf = new mPDF('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10);
                break;
            default:
                $mpdf = new mPDF('utf-8', 'A4', '8', '', 10, 10, 7, 7, 10, 10);
                break;
        }

        //get style sheet
        $stylesheet = file_get_contents(Mage::getModuleDir('lib', 'Webinse_OrderPdf') . '/lib/mpdf/style.css');

        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->list_indent_first_level = 0;

        //forming pdf
        $mpdf->WriteHTML($html, 2);
        $mpdf->Output((string) $documentName . P_EXT, 'I');
        exit();
    }

}