<?php
/**
 * Created by PhpStorm.
 * User: mikhail
 * Date: 7/21/14
 * Time: 4:41 PM
 */

class Webinse_OrderPdf_Model_System_Config_Source_Orientation
{
    public function toOptionArray()
    {
        $helper = Mage::helper('wb_orderpdf');
        return array(
            array('value' => 'L', 'label' => $helper->__('Landscape')),
            array('value' => 'P', 'label' => $helper->__('Portrait')),
        );
    }
} 