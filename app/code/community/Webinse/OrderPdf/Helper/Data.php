<?php

/**
 * Data helper
 *
 * @category   Webinse
 * @package    Webinse_OrderPdf
 * @author     Webinse Team <alena.tsareva@webinse.com>
 */
class Webinse_OrderPdf_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * Retrieve order visible blocks from sysytem config
     *
     * @return array
     */
    public function getOrderVisibleBlocks()
    {
        $visibleBlocks = unserialize(Mage::getStoreConfig('orderpdf/general/item'));

        return $visibleBlocks;
    }

    private function _getConfig($group, $field)
    {
        return Mage::getStoreConfig('orderpdf/' . $group . '/' . $field);
    }

    /**
     * Retrieve pdf styles
     *
     * @param string $field
     * @return string
     */
    public function getOrderPdfStyles($field)
    {
        return $this->_getConfig('theme', $field);
    }

    /**
     * Retrieve pdf footer styles
     *
     * @param string $field
     * @return string
     */
    public function getOrderPdfFooterStyles($field)
    {
        return $this->_getConfig('footer', $field);
    }

    /**
     * Retrieve company information
     *
     * @param string $field
     * @return string
     */
    public function getOrderPdfCompanyInfo($field)
    {
        return $this->_getConfig('company', $field);
    }

    /**
     * Retrieve size from string
     *
     * @param string $string
     * @return array|bool
     */
    public function parseSize($string)
    {
        $size = explode('x', strtolower($string));
        if (sizeof($size) == 2) {
            return array(
                'width'  => ($size[0] > 0) ? $size[0] : null,
                'height' => ($size[1] > 0) ? $size[1] : null,
            );
        }
        return false;
    }

}
