<?php
class Swatches_CustomOptions_Block_Adminhtml_Globalcustomoptions_Group_Edit_Tab_Swatches
    extends Swatches_CustomOptions_Block_Adminhtml_Catalog_Product_Edit_Tab_Swatches
{
    protected function _getGroup()
    {
        return Mage::registry('global_custom_options_group');
    }

    public function canShowTab()
    {
        return (bool)$this->getOptionsCollection()->count();
    }

    public function getSubmitUrl()
    {
        $urlData = array(
            'id'    => $this->_getGroup()->getId(),
            'group' => true,
        );
        return Mage::helper('adminhtml')
            ->getUrl('*/swatches_customoptions/upload', $urlData);
    }

    public function getOptionsCollection()
    {
        if (is_null($this->_optionsCollection)) {
            $this->_optionsCollection = Mage::getModel('global_custom_options/group_option')
                ->getGroupOptionCollection($this->_getGroup());
            foreach ($this->_optionsCollection as $key => $item) {
                if (!in_array($item->getType(), $this->_getSelectOptionTypes())) {
                    $this->_optionsCollection->removeItemByKey($key);
                }
            }
        }
        return $this->_optionsCollection;
    }
}
