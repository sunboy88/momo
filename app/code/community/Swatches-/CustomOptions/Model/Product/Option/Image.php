<?php
class Swatches_CustomOptions_Model_Product_Option_Image
    extends Swatches_CustomOptions_Model_Product_Option_Image_Abstract
{
    protected function _construct()
    {
        $this->_init('swatches_customoptions/product_option_image');
    }
}
