<?php
if (version_compare(Mage::getVersion(), '1.6.0', '>')) {
    abstract class Swatches_CustomOptions_Model_Resource_Product_Option_Abstract
        extends Mage_Core_Model_Resource_Db_Abstract {};
}
else {
    abstract class Swatches_CustomOptions_Model_Resource_Product_Option_Abstract
        extends Mage_Core_Model_Mysql4_Abstract {};
}

class Swatches_CustomOptions_Model_Resource_Product_Option_Image 
    extends Swatches_CustomOptions_Model_Resource_Product_Option_Abstract
{
    protected function _construct()
    {
        $this->_init('swatches_customoptions/images', 'image_id');
    }
    
    public function deleteImages($option_type_ids = array())
    {
        if ($option_type_ids) {
            $this->_getWriteAdapter()->delete(
                $this->getMainTable(),
                array('option_type_id IN (?)' => $option_type_ids)
            );
        }
        return $this;
    }

    public function copySwatchFromGlobalGroup($globalOptionValue, $productOptionValue)
    {
        $write  = $this->_getWriteAdapter();
        $read   = $this->_getReadAdapter();

        $fromTable = $this->getTable('global_custom_options/swatches');
        $toTable   = $this->getTable('swatches_customoptions/images');

        $select = $read->select()
            ->from($fromTable, array(new Zend_Db_Expr($productOptionValue->getId()), 'store_id', 'image'))
            ->where('option_type_id = ? AND store_id = 0', $globalOptionValue->getId());

        $insertSelect = $write->insertFromSelect(
            $select,
            $toTable,
            array('option_type_id', 'store_id', 'image'),
            Varien_Db_Adapter_Interface::INSERT_ON_DUPLICATE
        );

        $write->query($insertSelect);
    }
}
