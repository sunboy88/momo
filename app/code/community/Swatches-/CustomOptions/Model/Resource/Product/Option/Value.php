<?php
class Swatches_CustomOptions_Model_Resource_Product_Option_Value extends Mage_Catalog_Model_Resource_Product_Option_Value
{
    /**
     * Duplicate product options value
     *
     * @param Mage_Catalog_Model_Product_Option_Value $object
     * @param int $oldOptionId
     * @param int $newOptionId
     * @return Mage_Catalog_Model_Product_Option_Value
     */
    public function duplicate(Mage_Catalog_Model_Product_Option_Value $object, $oldOptionId, $newOptionId)
    {
        $writeAdapter = $this->_getWriteAdapter();
        $readAdapter  = $this->_getReadAdapter();
        $select       = $readAdapter->select()
            ->from($this->getMainTable())
            ->where('option_id = ?', $oldOptionId);
        $valueData = $readAdapter->fetchAll($select);

        $valueCond = array();

        foreach ($valueData as $data) {
            $optionTypeId = $data[$this->getIdFieldName()];
            unset($data[$this->getIdFieldName()]);
            $data['option_id'] = $newOptionId;

            $writeAdapter->insert($this->getMainTable(), $data);
            $valueCond[$optionTypeId] = $writeAdapter->lastInsertId($this->getMainTable());
        }

        unset($valueData);

        foreach ($valueCond as $oldTypeId => $newTypeId) {
            // price
            $priceTable = $this->getTable('catalog/product_option_type_price');
            $columns= array(
                new Zend_Db_Expr($newTypeId),
                'store_id', 'price', 'price_type'
            );

            $select = $readAdapter->select()
                ->from($priceTable, array())
                ->where('option_type_id = ?', $oldTypeId)
                ->columns($columns);
            $insertSelect = $writeAdapter->insertFromSelect($select, $priceTable,
                array('option_type_id', 'store_id', 'price', 'price_type'));
            $writeAdapter->query($insertSelect);

            // title
            $titleTable = $this->getTable('catalog/product_option_type_title');
            $columns= array(
                new Zend_Db_Expr($newTypeId),
                'store_id', 'title'
            );

            $select = $this->_getReadAdapter()->select()
                ->from($titleTable, array())
                ->where('option_type_id = ?', $oldTypeId)
                ->columns($columns);
            $insertSelect = $writeAdapter->insertFromSelect($select, $titleTable,
                array('option_type_id', 'store_id', 'title'));
            $writeAdapter->query($insertSelect);
        }

        $select = $readAdapter->select()
            ->from($this->getTable('swatches_customoptions/images'))
            ->where('option_type_id IN (?)', array_keys($valueCond));
        $swatchesData = $readAdapter->fetchAll($select);
        foreach ($swatchesData as $data) {
            if ($valueCond[$data['option_type_id']]) {
                $data['option_type_id'] = $valueCond[$data['option_type_id']];
                unset($data['image_id']);
                $writeAdapter->insert($this->getTable('swatches_customoptions/images'), $data);
            }
        }

        return $object;
    }
}
